package Main;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by mateus on 9/28/14.
 */
public abstract class LispList<T> {
    public static <T> LispList<T> cons(T head, T tail) {
        return new LispElement<T>(new Atom<T>(head), new Atom<T>(tail));
    }

    public static <T> LispList<T> cons(T head, LispList<T> tail) {
        return new LispElement<T>(new Atom<T>(head), tail);
    }

    public static <T> LispList<T> cons(LispList<T> head, T tail) {
        return new LispElement<T>(head, new Atom<T>(tail));
    }

    public static <T> LispList<T> cons(LispList<T> head, LispList<T> tail) {
        return new LispElement<T>(head, tail);
    }

    public static void main(String[] args) {
        LispList<String> l = LispList.cons("A", "B");
        l = LispList.cons(l, "C");
        l = LispList.cons("D", l);
        LispList<Number> l2 = LispList.cons(1, 2);
        l2 = LispList.cons(l2, 3);
        l2 = LispList.cons(4, l2);
        System.out.println(l);
        System.out.println(l2);

    }

    public LispList<T> car() {
        throw new NotImplementedException();
    }

    public LispList<T> cdr() {
        throw new NotImplementedException();
    }
}

class LispElement<T> extends LispList<T> {
    private LispList<T> head, tail;

    LispElement(LispList<T> head, LispList<T> tail) {
        this.head = head;
        this.tail = tail;
    }

    public LispList<T> car() {
        return this.head;
    }

    public LispList<T> cdr() {
        return this.tail;
    }

    public String toString() {
        return "(" + this.head + "," + this.tail + ")";
    }
}

class Atom<T> extends LispList<T> {
    private T data;

    Atom(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return this.data.toString();
    }
}
