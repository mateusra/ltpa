package Main;

import java.util.AbstractList;
import java.util.List;

/**
 * Created by mateus on 9/28/14.
 */
public class CircularList<E> extends AbstractList<E> implements List<E> {

    private int size;
    private Element<E> anchor;

    public CircularList() {
        this.anchor = null;
        this.size = 0;
    }

    @Override
    public void add(int index, E e) {
        if (index > size) throw new IndexOutOfBoundsException("index = " + index + ", but size = " + this.size);
        if (this.anchor == null) {
            this.anchor = new Element<E>(e);
            this.anchor.setNext(this.anchor);
            this.anchor.setLast(this.anchor);
        } else {
            Element<E> actual = this.goTo((index == size) ? index - 1 : index), element = new Element<E>(e);
            element.setLast((index == size) ? actual : actual.getLast());
            element.setNext((index == size) ? actual.getNext() : actual);
            element.getNext().setLast(element);
            element.getLast().setNext(element);
        }
        this.size++;
    }

    private Element<E> goTo(int index) {
        Element<E> actual = (index > size / 2) ? (anchor.getLast()) : (anchor);
        for (int i = (index > size / 2) ? (size - 1) : (0); i != index; i += (index > size / 2) ? (-1) : (+1)) actual = actual.getNext();
        return actual;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public E get(int index) {
        if (index >= size) throw new IndexOutOfBoundsException("index = " + index + ", but size = " + this.size);
        return this.goTo(index).getId();
    }

    private class Element<T> {

        private T id;
        private Element<T> last, next;

        private Element(T id) {
            this.id = id;
            this.last = null;
            this.next = null;
        }

        public T getId() {
            return id;
        }

        public Element<T> getLast() {
            return last;
        }

        public void setLast(Element<T> last) {
            this.last = last;
        }

        public Element<T> getNext() {
            return next;
        }

        public void setNext(Element<T> next) {
            this.next = next;
        }
    }
}
