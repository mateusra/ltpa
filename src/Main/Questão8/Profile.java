package Main.Questão8;

import java.io.IOException;

/**
 * Created by Mateus on 10/10/2014.
 */
public abstract class Profile<T extends Document> {
    private T document;

    protected Profile(T document) {
        this.document = document;
    }

    protected Profile() {
    }

    public void setDocument(T document) {
        this.document = document;
    }

    public T getDocument() {
        return document;
    }

    public abstract void analyzeDocument() throws IOException;


}
