package Main.Questão8;

import java.io.IOException;

/**
 * Created by Mateus on 10/10/2014.
 */
public abstract class KGramsProfile extends Profile<TextDocument> {
    private static final int MAX_LENGTH = 100000;
    private double[] histogram;
    private int level;

    public double[] getHistogram() {
        return histogram;
    }

    public KGramsProfile(int level) {
        this.level = level;
        this.histogram = new double[KGramsProfile.MAX_LENGTH];
    }

    @Override
    public void analyzeDocument() throws IOException {
        super.getDocument().open();
        String line;
        while ((line = super.getDocument().nextLine()) != null) {
            while (line.length() > level) {
                this.histogram[(Math.abs(line.substring(0, this.level).hashCode()) % KGramsProfile.MAX_LENGTH)]++;
                line = line.substring(1);
            }
            this.histogram[(Math.abs(line.hashCode()) % KGramsProfile.MAX_LENGTH)]++;
        }

    }
}
