package Main.Questão8;

import java.io.File;

/**
 * Created by Mateus on 10/10/2014.
 */
public abstract class Document {

    private File file;
    private Profile profile;

    protected Document(File file, Profile profile) {
        this.file = file;
        this.profile = profile;
    }

    protected File getFile() {
        return file;
    }
}
