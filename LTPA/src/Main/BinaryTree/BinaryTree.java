package Main.BinaryTree;

import java.util.*;

/**
 * Created by mateus on 9/28/14.
 */
public class BinaryTree<T> {

    private Node<T> root;
    private Comparator<T> cmp;

    public BinaryTree(Comparator<T> cmp) {
        this.cmp = cmp;
        this.root = null;
    }

    public void add(T elem) {
        if (root == null) {
            this.root = new Node<T>(elem);
        } else {
            this.add(elem, root);
        }
    }

    private void add(T elem, Node<T> root) {
        if (cmp.compare(elem, root.getId()) >= 0) {
            if (root.getLeft() == null) {
                root.setLeft(new Node<T>(elem));
            } else {
                add(elem, root.getLeft());
            }
        } else if (cmp.compare(elem, root.getId()) < 0) {
            if (root.getRight() == null) {
                root.setRight(new Node<T>(elem));
            } else {
                add(elem, root.getRight());
            }
        }
    }

    public Iterator<T> iteratorBreadthFirst() {
        return new BinaryTreeIteratorBreadthFirst<T>(root.clone());
    }

    public Iterator<T> iteratorPreOrdem() {
        return new BinaryTreeIteratorPreOrdem<T>(root.clone());
    }

    public Iterator<T> iteratorPosOrdem() {
        return new BinaryTreeIteratorPosOrdem<T>(root.clone());
    }

    public Iterator<T> iteratorInOrdem() {
        return new BinaryTreeIteratorInOrdem<T>(root.clone());
    }

    public void print() {
        this.root.print(1);
    }

    class Node<T> {

        private T id;
        private Node<T> left, right;

        private Node(T id) {
            this.id = id;
            this.left = null;
            this.right = null;
        }

        public T getId() {
            return id;
        }

        public Node<T> getLeft() {

            return left;
        }

        public void setLeft(Node<T> left) {
            this.left = left;
        }

        public Node<T> getRight() {
            return right;
        }

        public void setRight(Node<T> right) {
            this.right = right;
        }

        public Node<T> clone() {
            Node<T> c = new Node<>(this.id);
            c.setLeft((this.left != null) ? this.left.clone() : null);
            c.setRight((this.right != null) ? this.right.clone() : null);
            return c;
        }

        public boolean equals(Node<T> elem) {
            return (elem == null) ? false : this.id.equals(elem.getId());
        }

        public void print(int nivel) {
            if (this.right != null) {
                this.right.print(nivel + 1);
            }
            if (this.left != null) {
                this.left.print(nivel + 1);
            }
            System.out.print(this.id + " ");

        }
    }

    class BinaryTreeIteratorBreadthFirst<T> implements Iterator {

        private Deque<Node<T>> queue;

        BinaryTreeIteratorBreadthFirst(Node<T> root) {
            this.queue = new ArrayDeque<>();
            this.queue.add(root);
        }

        @Override
        public boolean hasNext() {
            return !this.queue.isEmpty();
        }

        @Override
        public T next() {
            Node<T> next = this.queue.removeFirst();
            if (next.getRight() != null) {
                this.queue.addLast(next.getRight());
            }
            if (next.getLeft() != null) {
                this.queue.addLast(next.getLeft());
            }
            return next.getId();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    class BinaryTreeIteratorPreOrdem<T> implements Iterator {

        private List<Node<T>> queue;

        BinaryTreeIteratorPreOrdem(Node<T> root) {
            this.queue = new ArrayList<>();
            do {
                this.queue.add(root);
            } while ((root = root.getRight()) != null);
        }

        @Override
        public boolean hasNext() {
            return !this.queue.isEmpty();
        }

        @Override
        public T next() {
            Node<T> next = this.queue.get(0);
            List<Node<T>> betwin = new ArrayList<>();
            next = next.getLeft();
            while (next != null) {
                betwin.add(next);
                next = next.getRight();
            }
            next = this.queue.get(0);
            while (next.getRight() != null) {
                next = next.getRight();
            }
            this.queue.addAll(this.queue.indexOf(next) + 1, betwin);
            return this.queue.remove(0).getId();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    class BinaryTreeIteratorInOrdem<T> implements Iterator {

        private Deque<Node<T>> queue;

        BinaryTreeIteratorInOrdem(Node<T> root) {
            this.queue = new ArrayDeque<>();
            do {
                this.queue.add(root);
            } while ((root = root.getRight()) != null);
        }

        @Override
        public boolean hasNext() {
            return !this.queue.isEmpty();
        }

        @Override
        public T next() {
            Node<T> next = this.queue.removeLast();
            T id = next.getId();
            next = next.getLeft();
            while (next != null) {
                this.queue.add(next);
                next = next.getRight();
            }
            return id;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    class BinaryTreeIteratorPosOrdem<T> implements Iterator {

        private Deque<Node<T>> queue;
        private Node<T> last;

        BinaryTreeIteratorPosOrdem(Node<T> root) {
            this.queue = new ArrayDeque<>();
            do {
                this.queue.push(root);
            } while ((root = root.getRight()) != null);

        }

        @Override
        public boolean hasNext() {
            return !this.queue.isEmpty();
        }

        @Override
        public T next() {
            Node<T> next = this.queue.peek();
            if ((next = next.getLeft()) != null && !next.equals(last)) {
                do this.queue.push(next); while ((next = next.getRight()) != null);
                return this.next();
            } else {
                this.last = this.queue.pop();
                return this.last.getId();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
