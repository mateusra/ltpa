package Main;

import Main.Questão8.TenGramProfile;
import Main.Questão8.TenGramProfileComparator;
import Main.Questão8.TextDocument;

import java.io.File;
import java.io.IOException;

/**
 * Created by Mateus on 10/10/2014.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        TenGramProfile p0 = new TenGramProfile(), p1 = new TenGramProfile(), p2 = new TenGramProfile();
        TextDocument doc0 = new TextDocument(new File("lorem-0.txt"), p0), doc1 = new TextDocument(new File("lorem-1.txt"), p1), doc2 = new TextDocument(new File("lorem-2.txt"), p2);
        TenGramProfileComparator c = new TenGramProfileComparator();
        p0.setDocument(doc0);
        p0.analyzeDocument();
        p1.setDocument(doc1);
        p1.analyzeDocument();
        p2.setDocument(doc2);
        p2.analyzeDocument();

        System.out.print(String.format("%1$.2f", c.compare(p0, p0)*100) + "%\t");
        System.out.print(String.format("%1$.2f", c.compare(p0, p1)*100) + "%\t");
        System.out.print(String.format("%1$.2f", c.compare(p0, p2)*100) + "%\n");

        System.out.print(String.format("%1$.2f", c.compare(p1, p0)*100) + "%\t");
        System.out.print(String.format("%1$.2f", c.compare(p1, p1)*100) + "%\t");
        System.out.print(String.format("%1$.2f", c.compare(p1, p2)*100) + "%\n");

        System.out.print(String.format("%1$.2f", c.compare(p2, p0)*100) + "%\t");
        System.out.print(String.format("%1$.2f", c.compare(p2, p1)*100) + "%\t");
        System.out.print(String.format("%1$.2f", c.compare(p2, p2)*100) + "%\n");
    }
}
