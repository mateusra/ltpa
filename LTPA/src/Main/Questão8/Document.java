package Main.Questão8;

import java.io.File;

/**
 * Created by Mateus on 10/10/2014.
 */
public abstract class Document {

    private File file;
    private Profile profile;

    public Document(File file, Profile profile) {
        this.file = file;
        this.profile = profile;
    }

    public File getFile() {
        return file;
    }

    public Profile getProfile() {
        return profile;
    }
    
}
