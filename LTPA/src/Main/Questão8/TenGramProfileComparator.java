package Main.Questão8;

/**
 * Created by Mateus on 10/10/2014.
 */
public class TenGramProfileComparator implements ProfileComparator<TenGramProfile>{

    @Override
    public double compare(TenGramProfile profileA, TenGramProfile profileB) {
        double produto = 0, normaA = 0, normaB = 0;
        for (int i = 0; i < profileA.getHistogram().length; i++) {
            produto += profileA.getHistogram()[i] * profileB.getHistogram()[i];
            normaA += Math.pow(profileA.getHistogram()[i], 2);
            normaB += Math.pow(profileB.getHistogram()[i], 2);
        }
        produto /= (Math.sqrt(normaA) * Math.sqrt(normaB));
        return produto;
    }
}
