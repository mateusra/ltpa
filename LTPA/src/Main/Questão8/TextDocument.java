package Main.Questão8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Mateus on 10/10/2014.
 */
public class TextDocument extends Document {
    private BufferedReader reader;

    public TextDocument(File file, Profile profile) {
        super(file, profile);
    }

    public String nextLine() throws IOException {
        return this.reader.readLine();
    }

    public void open() throws IOException {
        this.reader = new BufferedReader(new FileReader(super.getFile()));
    }

    public void close() throws IOException {
        this.reader.close();
    }
}
