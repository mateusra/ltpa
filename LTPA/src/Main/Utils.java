package Main;

import Main.BinaryTree.BinaryTree;

import java.util.*;

/**
 * Created by mateus on 9/27/14.
 */
public class Utils {
//
//    public static <T> List<T> heapsort(List<? extends T> l, Comparator<T> c) {
//        Queue<T> p = new PriorityQueue<>(c);
//        p.addAll(l);
//        List<T> sorted = new ArrayList<T>();
//        while (p.isEmpty()) sorted.add(p.remove());
//        return sorted;
//    }

    public static <T> List<T> sort(List<T> l, Comparator<? super T> c) {
        if (l.size() > 1) {
            ListIterator<T> iLeft = sort(l.subList(0, l.size() / 2), c).listIterator(), iRight = sort(l.subList(l.size() / 2, l.size()), c).listIterator();
            l = new ArrayList<>(l.size());
            while (iLeft.hasNext() && iRight.hasNext()) {
                T a = iLeft.next(), b = iRight.next();
                int compare = c.compare(a, b);
                if (compare == 0) {
                    l.add(a);
                    l.add(b);
                } else if (compare > 0) {
                    l.add(b);
                    iLeft.previous();
                } else {
                    l.add(a);
                    iRight.previous();
                }
            }
            while (iLeft.hasNext()) l.add(iLeft.next());
            while (iRight.hasNext()) l.add(iRight.next());
        }
        return l;
    }

    public static void main(String[] args) {
        BinaryTree< Character> tree = new BinaryTree<>(new Comparator<Character>() {

            @Override
            public int compare(Character t, Character t1) {
                return t.compareTo(t1);
            }
        });
        tree.add('D');
        tree.add('C');
        tree.add('E');
        tree.add('F');
        tree.add('A');
        tree.add('Z');
        tree.add('K');
        tree.add('P');
        tree.add('W');
        tree.print();
        System.out.println();
        System.out.println("InOrdem");
        Iterator<Character> i = tree.iteratorInOrdem();
        while (i.hasNext()) {
            System.out.print(i.next() + "-");
        }

        System.out.println();
        System.out.println("Largura");
        i = tree.iteratorBreadthFirst();
        while (i.hasNext()) {
            System.out.print(i.next() + "-");
        }
        System.out.println();
        System.out.println("PreOrdem");
        i = tree.iteratorPreOrdem();
        while (i.hasNext()) {
            System.out.print(i.next() + "-");
        } 
        System.out.println();
        System.out.println("PosOrdem");
        i = tree.iteratorPosOrdem();
        while (i.hasNext()) {
            System.out.print(i.next() + "-");
        }
        System.out.println();

    }
}
